﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WpfApp1.ViewModel
{
    class ViewModel : Bindable
    {
       
        LinkStorage ls = new LinkStorage();
        public ObservableCollection<link> links;
        private string url;
        private string topic;
        private string name;
        private link selectedLink;
        private bool status;


        public ObservableCollection<link> Links { get {
                if (status)
                {
                    return ls.GetLinksSortedByTopic();
                }
                else
                {
                    return ls.GetLinksSortedByName();
                }
            } set => links = value; }


        public ICommand AddCommand { get; set; }

        public ICommand RemoveCommand { get; set; }

        public ICommand OpenCommand { get; set; }

        public ICommand SortCommand { get; set; }
        public string Url { get => url; set => url = value; }
        public string Topic { get => topic; set => topic = value; }
        public string Name { get => name; set => name = value; }
        public link SelectedLink { get => selectedLink; set => selectedLink = value; }
        public string Text { get => Links.Count.ToString() + " Links has been added"; }

        public ViewModel()
        {
            //links = new ObservableCollection<link>();
            status = false;
            AddCommand = new RelayCommand(AddMethod);
            RemoveCommand = new RelayCommand(RemoveMethod, x => selectedLink != null);
            OpenCommand = new RelayCommand(OpenLink, x => selectedLink != null);
            SortCommand = new RelayCommand(SortMethod, x => Links.Count > 1);
        }

        public void AddMethod(object o)
        {
            ls.AddLink(Url, Name, Topic);
            OnPropertyChange("Links");
        }

        public void RemoveMethod(object o)
        {
            ls.RemoveLink(selectedLink);
            OnPropertyChange("Links");
        }

        public void SortMethod(object o) {
            if (status == false)
            {
                status = true;
                Links = ls.GetLinksSortedByTopic();
            }
            else
            {
                status = false;
                Links = ls.GetLinksSortedByName();
            }
            OnPropertyChange("Links");

        }

        public void OpenLink(object o)
        {
            System.Diagnostics.Process.Start(selectedLink.url);
        }
    }
}
