﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1
{
    class LinkStorage
    {
        ThesisLinksEntitieslink en;

        public LinkStorage()
        {
            this.en = new ThesisLinksEntitieslink();
        }

        public IEnumerable<link> GetLinks
        {
            get
            {
                return en.links;
            }
        }

        public void AddLink(string url, string name, string topic) {
            link l = new link()
            {
                topic = topic,
                url = url,
                nme = name
            };
            en.links.Add(l);
            en.SaveChanges();
        }

        public void RemoveLink(link link)
        {
            en.links.Attach(link);
            en.links.Remove(link);
            en.SaveChanges();
        }

        public ObservableCollection<link> GetLinksSortedByName()
        {
            ObservableCollection<link> links = new ObservableCollection<link>();
            foreach (var item in GetLinks)
            {
                if (item !=null)
                {
                    links.Add(item); 
                }
            }
            return new ObservableCollection<link>(links.OrderBy(x => x.nme));
        }

        public ObservableCollection<link> GetLinksSortedByTopic()
        {
            ObservableCollection<link> links = new ObservableCollection<link>();
            foreach (var item in GetLinks)
            {
                if (item != null)
                {
                    links.Add(item);
                }
            }
            return new ObservableCollection<link>(links.OrderBy(x => x.topic));
        }
    }
}
